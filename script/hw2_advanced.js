class Hamburger { // задаємо клас 'Hamburger'
  
  constructor(size, stuffing) { // задаємо конструктор
    try {
      if(!size){
        throw new Error("No size given")
      }
      if(!stuffing){
        throw new Error("No stuffing added")
      }
        
    this.size = size;
    this.stuffing = stuffing;
    this.Toppings = [];
    } 
    catch (e) {
      console.log(e.message);
    }
  }

  
  get burgerSize() {
    return this.size;
  }

  set burgerSize(value) {
    this.size = value
  }

  get burgerStuffing() {
    return this.stuffing
  }

  set burgerStuffing(value) {
    this.stuffing = value
  }

  getBurgerToppings() {
    return this.Toppings;
  }
  
  addTopping(topping) { // щоб ДОДАТИ ДОБАВКУ гамбургеру:
    try {
      if (!this.getBurgerToppings().includes(topping)){ // якщо не включає добавку
        return this.getBurgerToppings().push(topping)   // тоді додати її 
      }
      if (this.getBurgerToppings().includes(topping)){
        throw new Error("dublicate topping");
      }
      else {
        throw new Error("No toppings added");
      }
    }
    catch (e){
      console.log(e.message);
    }
  }


  removeTopping(topping) { // шоб ЗАБРАТИ ДОБАВКУ, якщо вона раніше була додана
    try {
      if (this.getBurgerToppings().indexOf(topping)!== -1){
        return this.getBurgerToppings().splice(this.getBurgerToppings().indexOf(topping));
      }
      else {
        throw new HamburgerException ("topping is not correctly removed");
      }
    } catch (e){
      console.log(e.message);
    }
  }


  calculatePrice () { // щоб вирахувати/отримати ЦІНУ гамбургера
    return this.getBurgerToppings().reduce(function(acc, prices) {return acc + prices.price}, 0)
      + this.burgerSize.price + this.burgerStuffing.price
  }


  calculateCalories () { // щоб вирахувати/отримати загальну кількість КАЛОРІЇВ замовленого гамбургера
    return this.getBurgerToppings().reduce(function(acc, calories) {return acc + calories.calories}, 0)
    + this.burgerSize.calories + this.burgerStuffing.calories
  }

}


/* Визначаємо розміри, види начинок та добавок;
задаємо їм значення ціни та калорійності */

Hamburger.SIZE_SMALL = {
  price: 50,
  calories: 20
};
Hamburger.SIZE_LARGE = {
  price: 100,
  calories: 40
};
Hamburger.STUFFING_CHEESE = {
  price: 10,
  calories: 20
};
Hamburger.STUFFING_SALAD = {
  price: 20,
  calories: 5
};
Hamburger.STUFFING_POTATO = {
  price: 15,
  calories: 10
};
Hamburger.TOPPING_MAYO = {
  name: "TOPPING_MAYO",
  price: 20,
  calories: 5
};
Hamburger.TOPPING_SPICE = {
  name: "TOPPING_SPICE",
  price: 15,
  calories: 0
};



/*        ПЕРЕВІРКА РОБОТИ КЛАСУ  'Hamburger'        */

let hamburger = new Hamburger(Hamburger.SIZE_SMALL, Hamburger.STUFFING_CHEESE); // замовили маленький гамбургер з сирною начинкою

hamburger.addTopping(Hamburger.TOPPING_MAYO); // додали до гамбургера добавку з майонезу

console.log("Calories: %f", hamburger.calculateCalories()); // отримали підраховане значення калоріїв

console.log("Price: %f", hamburger.calculatePrice()); // отримали ціну замовленого гамбургера

hamburger.addTopping(Hamburger.TOPPING_SPICE); // додали до гамбургера добавку з приправ

console.log("Price with sauce: %f", hamburger.calculatePrice()); // отримали ціну з врахуванням доданої приправи

console.log("Is hamburger large: %s", hamburger.burgerSize === Hamburger.SIZE_LARGE); // перевіримо, чи великий гамбургер?

hamburger.removeTopping(Hamburger.TOPPING_SPICE); // забрали приправу з гамбургера

console.log("Have %d toppings", hamburger.getBurgerToppings().length); // повинен залишитись з добавок тільки один майонез

let h2 = new Hamburger(); // а тут не вказали при замовленні основні параметри гамбургера

let h3 = new Hamburger(Hamburger.SIZE_SMALL); // а тут не вказали при замовленні основні параметри гамбургера


let h5 = new Hamburger(Hamburger.TOPPING_SPICE, Hamburger.TOPPING_SPICE); // а тут помилились вказавши замість розміру приправу